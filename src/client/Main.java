package client;

import javax.naming.InitialContext;
import javax.naming.NamingException ;

import ejb.entites.Ordinateur;
import ejb.entites.Installation;
import ejb.sessions.IGestionParc;

public class Main {

	public static void main(String[] args) {
		InitialContext ctx;
		try {
			ctx = new InitialContext();
			Object obj ;
			obj = ctx.lookup("ejb:polyJPA/polyJPASessions//GestionParc!ejb.sessions.IGestionParc");
		    IGestionParc session = (IGestionParc) obj ;
		      
		    System.out.println("*** Création ordinateur ***") ;
		    System.out.println("code ordinateur:"+session.testGeneratedValue());
		    System.out.println("*** Initialisation du parc ***") ;
		    session.initParc();
		    System.out.println("*** Affichage des ordinateurs (hiérarchie héritage) ***") ;
		    for(Ordinateur o : session.testHierarchie())
		    	System.out.println(" - Ordinateur :"+o.getNom());
		    System.out.println("*** Les installations de ordi 2 ***") ;
		    for (Installation i : session.testOneToMany(2)) 
		    	System.out.println(" - installation i :"+i.getCodeInstall());
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}      
	}
}
