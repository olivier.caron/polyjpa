package ejb.sessions;

import java.util.Collection;

import ejb.entites.Installation;
import ejb.entites.Ordinateur;

@jakarta.ejb.Remote
public interface IGestionParc {
  public int testGeneratedValue() ;
  public void initParc() ;
  public Collection<Ordinateur> testHierarchie();
  public Collection<Installation> testOneToMany(int codeOrdi);
}
