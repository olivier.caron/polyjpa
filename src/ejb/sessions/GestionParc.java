package ejb.sessions;

import java.util.Collection;
import java.util.HashSet;

import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;

import ejb.entites.Installation;
import ejb.entites.Logiciel;
import ejb.entites.Mac;
import ejb.entites.Ordinateur;
import ejb.entites.PC;


@Stateless
public class GestionParc implements IGestionParc {
    @PersistenceContext(unitName="polyJPA-unit") EntityManager em ;
	public GestionParc() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public int testGeneratedValue() {
		Ordinateur ordi=new Mac() ;
		ordi.setNom("MAC caron");
		ordi.setNumeroIP("192.168.0.2");
		em.persist(ordi) ;
		System.out.println("test generated value: "+ordi.getCode());
		return ordi.getCode();
	}
	
	

	@Override
	public void initParc() {
		Ordinateur ordi1=new PC() ; 
		ordi1.setNom("phinaert03"); ordi1.setNumeroIP("192.168.0.4"); em.persist(ordi1) ;
		Ordinateur ordi2=new PC() ; 
		ordi2.setNom("phinaert04"); ordi2.setNumeroIP("192.168.0.5"); em.persist(ordi2) ;
		Logiciel l1,l2,l3 ;
		l1= new Logiciel("Tomb Raider I",1) ; em.persist(l1) ;
		l2= new Logiciel("Tomb raider II",2) ; em.persist(l2);
		l3= new Logiciel("MS Excel", 3) ; em.persist(l3);
		Installation i1,i2,i3 ;
		i1= new Installation() ;
		i1.setDateInstall(java.sql.Date.valueOf("2019-03-27"));
		i1.setVersion("1.0"); i1.setCodeInstall(1); i1.setLogiciel(l1);
		em.persist(i1);
		i2= new Installation() ;
		i2.setDateInstall(java.sql.Date.valueOf("2019-03-27"));
		i2.setVersion("2.0"); i2.setCodeInstall(2); i2.setLogiciel(l2);
		em.persist(i2);
		ordi1.setLogicielsInstalles(new HashSet<Installation>());
		ordi1.getLogicielsInstalles().add(i1);
		ordi1.getLogicielsInstalles().add(i2);
		i3= new Installation() ;
		i3.setDateInstall(java.sql.Date.valueOf("2019-01-02"));
		i3.setVersion("5.0"); i3.setCodeInstall(3); i3.setLogiciel(l3);
		em.persist(i3);
		ordi2.setLogicielsInstalles(new HashSet<Installation>());
		ordi2.getLogicielsInstalles().add(i3);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Collection<Ordinateur> testHierarchie() {
		Query q = em.createQuery("from Ordinateur As o") ;
		/* autres requêtes JPQL possibles :
		 * select o from Ordinateur o
		 * from Ordinateur
		 * from Ordinateur o
		 * from Ordinateur As o
		 */
		return (Collection<Ordinateur>) q.getResultList();
	}
	@SuppressWarnings("unchecked")
	@Override
	public Collection<Installation> testOneToMany(int codeOrdi) {
		Query q = em.createQuery("select i from Ordinateur As o join o.logicielsInstalles i "+
	                             " where o.code = :code")
								.setParameter("code", codeOrdi);
		return (Collection<Installation>) q.getResultList();
	}
	
	/*public Collection<Ordinateur> testjointures() {
		Query q = em.createQuery("select o from Installation Ordinateur As o join o.logicielsInstalles i "+
                " where o.code = :code");
return (Collection<Installation>) q.getResultList();
	}*/

}
