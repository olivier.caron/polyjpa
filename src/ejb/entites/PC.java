package ejb.entites;

import jakarta.persistence.Entity;

@Entity public class PC extends Ordinateur {

	private static final long serialVersionUID = 1L;
	private boolean withLinux ;
	
	public PC() {}

	public boolean isWithLinux() {
		return withLinux;
	}

	public void setWithLinux(boolean withLinux) {
		this.withLinux = withLinux;
	}

}
