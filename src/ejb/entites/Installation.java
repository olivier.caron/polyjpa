package ejb.entites;

import java.sql.Date;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne ;

@Entity
public class Installation implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	private Date dateInstall ;
	private String version ;
	@Id private int codeInstall ;
	@JoinColumn(name="ref_logiciel", nullable=false)
	@ManyToOne private Logiciel logiciel ;
	
	public Logiciel getLogiciel() {
		return logiciel;
	}

	public void setLogiciel(Logiciel logiciel) {
		this.logiciel = logiciel;
	}

	public Installation() {}

	public Date getDateInstall() {
		return dateInstall;
	}

	public void setDateInstall(Date dateInstall) {
		this.dateInstall = dateInstall;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public int getCodeInstall() {
		return codeInstall;
	}

	public void setCodeInstall(int codeInstall) {
		this.codeInstall = codeInstall;
	}
	
}
