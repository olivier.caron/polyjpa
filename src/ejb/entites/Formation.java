package ejb.entites;

import jakarta.persistence.Entity ;
import jakarta.persistence.Id ;


@Entity
public class Formation implements java.io.Serializable {
 
  private static final long serialVersionUID = 1L;
  @Id private int code ;
  private String nom ;
  
  public Formation() {}

public int getCode() {
	return code;
}

public void setCode(int code) {
	this.code = code;
}

public String getNom() {
	return nom;
}

public void setNom(String nom) {
	this.nom = nom;
}
  
}
