package ejb.entites;

import jakarta.persistence.*;


@DiscriminatorColumn
(name="type_ordi", discriminatorType=DiscriminatorType.STRING, length=30)
@DiscriminatorValue("ordinateur")
@Entity public abstract class Ordinateur implements java.io.Serializable {
	@GeneratedValue
	@Id private int code ;
	private String nom ;
	private String numeroIP ;
	@ManyToOne private Salle salle ;
	@OneToMany private java.util.Set<Installation> logicielsInstalles ;
	
	public Salle getSalle() {
		return salle;
	}

	public void setSalle(Salle salle) {
		this.salle = salle;
	}

	public java.util.Set<Installation> getLogicielsInstalles() {
		return logicielsInstalles;
	}

	public void setLogicielsInstalles(java.util.Set<Installation> logicielsInstalles) {
		this.logicielsInstalles = logicielsInstalles;
	}
	private static final long serialVersionUID = 1L;
	
	public Ordinateur() {}
	
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getNumeroIP() {
		return numeroIP;
	}
	public void setNumeroIP(String numeroIP) {
		this.numeroIP = numeroIP;
	}
	
	
	
  
}
