package ejb.entites;

import jakarta.persistence.*;

@Table(name="t_logiciel")
@Entity public class Logiciel  implements java.io.Serializable  {

	 private static final long serialVersionUID = 1L;
	 private String noma ;
	 private int codeLogiciel ;
	 private int truc ;
	
	public Logiciel() {}
	
	public Logiciel(String nom, int code) {
		this.noma=nom ;
		this.codeLogiciel=code ;
	}
	@Column(name="t_nom", length=40, unique=true, nullable=false)
	public String getNom() {
		return noma;
	}

	public void setNom(String nom) {
		this.noma = nom;
	}
	@Id @Column (name="code")
	public int getCodeLogiciel() {
		return codeLogiciel;
	}

	public void setCodeLogiciel(int codeLogiciel) {
		this.codeLogiciel = codeLogiciel;
	}


}