package ejb.entites;

import jakarta.persistence.Entity ;
import jakarta.persistence.Id ;
import jakarta.persistence.OneToMany ;
import jakarta.persistence.ManyToMany ;
import jakarta.persistence.JoinTable ;
import jakarta.persistence.JoinColumn ;

@Entity
public class Salle implements java.io.Serializable {
  /**
	 * 
	 */
  private static final long serialVersionUID = 1L;
	
  @Id private int code ;
  private String nom ;
  @OneToMany(mappedBy="salle") private java.util.Set<Ordinateur> ordinateurs ;
  @JoinTable(
		   name = "asso_salle_formations", 
		   joinColumns = @JoinColumn(name = "ref_salle"), 
		   inverseJoinColumns = @JoinColumn(name = "ref_formation")
		 )
  @ManyToMany private java.util.Set<Formation> formations ;
  public Salle() {}

public int getCode() {
	return code;
}

public void setCode(int code) {
	this.code = code;
}

public String getNom() {
	return nom;
}

public void setNom(String nom) {
	this.nom = nom;
}
  
}
