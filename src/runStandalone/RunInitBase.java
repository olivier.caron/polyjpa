
package runStandalone ;



import jakarta.persistence.* ;

public class RunInitBase {
 
   private EntityManagerFactory emf ;
   private EntityManager em ;
         
   public static void main(String[] args) {
       new RunInitBase().run() ;
   }
         
   public void run() {
     this.emf = Persistence.createEntityManagerFactory("polyJPAUnit");
     this.em = emf.createEntityManager();
   }
 }
