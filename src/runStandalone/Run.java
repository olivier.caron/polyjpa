
package runStandalone ;



import jakarta.persistence.* ;

public class Run {
 
   private EntityManagerFactory emf ;
   private EntityManager em ;
         
   public static void main(String[] args) throws InterruptedException {
       new Run().run() ;
   }
         
   public void run() throws InterruptedException{
     this.emf = Persistence.createEntityManagerFactory("polyJPAUnit");
     this.em = emf.createEntityManager();
     
     // création ordinateur MAC
     ejb.entites.Mac unMac=new ejb.entites.Mac() ;
     unMac.setCode(112);
     unMac.setNom("Mac aron");
     unMac.setNumeroIP("192.168.38.01");
     em.getTransaction().begin() ;
     em.persist(unMac);
     em.getTransaction().commit();
     // création logiciel
     ejb.entites.Logiciel unLogiciel=new ejb.entites.Logiciel("excel",1) ;
     


     em.getTransaction().begin() ;
     System.out.println("lire 1:") ;
     
     System.out.println("attendre 15sec") ;
     Thread.sleep( 15000);
     em.persist(unLogiciel);
     em.getTransaction().commit();
     em.getTransaction().begin() ;
     unLogiciel.setNom("word");
     System.out.println("attendre 15sec") ;
     Thread.sleep( 15000);
     em.getTransaction().commit();
   }
 }
